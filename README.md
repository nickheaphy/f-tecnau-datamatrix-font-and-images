# DataMatrix Font and Images for Tecnau Dynamic Perforator

The Tecnau Dynamic Perforator uses 90 different DataMatrix codes from 00 to 89.

## Images

Using `https://bitbucket.org/nickheaphy/f-prismaprepare-datamatrix-stamp-generator/src` a set of PNGs of all the DataMatrix codes from 00 to 89 has been generated for use in any Design Application.

It can be downloaded from this repository here: https://nickheaphy@bitbucket.org/nickheaphy/f-tecnau-datamatrix-font-and-images/raw/HEAD/DataMatrix%20Codes%2000%20to%2089%20(png).zip

## Font

This is a font that has each of these DataMatrix codes as a different character.

Download the font from: https://nickheaphy@bitbucket.org/nickheaphy/f-tecnau-datamatrix-font-and-images/raw/HEAD/TecnauDataMatrix_V2.otf 

The encoding is as follows:

| Keyboard | Datamatrix |
|:-------- |:---------- |
| 0 | 00 |
| 1 | 01 |
| 2 | 02 |
| 3 | 03 |
| 4 | 04 |
| 5 | 05 |
| 6 | 06 |
| 7 | 07 |
| 8 | 08 |
| 9 | 09 |
| ~ | 10 |
| ! | 11 |
| @ | 12 |
| # | 13 |
| $ | 14 |
| % | 15 |
| ^ | 16 |
| & | 17 |
| * | 18 |
| ( | 19 |
| ) | 20 |
| a | 21 |
| b | 22 |
| c | 23 |
| d | 24 |
| e | 25 |
| f | 26 |
| g | 27 |
| h | 28 |
| i | 29 |
| j | 30 |
| k | 31 |
| l | 32 |
| m | 33 |
| n | 34 |
| o | 35 |
| p | 36 |
| q | 37 |
| r | 38 |
| s | 39 |
| t | 40 |
| u | 41 |
| v | 42 |
| w | 43 |
| x | 44 |
| y | 45 |
| z | 46 |
| A | 47 |
| B | 48 |
| C | 49 |
| D | 50 |
| E | 51 |
| F | 52 |
| G | 53 |
| H | 54 |
| I | 55 |
| J | 56 |
| K | 57 |
| L | 58 |
| M | 59 |
| N | 60 |
| O | 61 |
| P | 62 |
| Q | 63 |
| R | 64 |
| S | 65 |
| T | 66 |
| U | 67 |
| V | 68 |
| W | 69 |
| X | 70 |
| Y | 71 |
| Z | 72 |
| < | 73 |
| > | 74 |
| , | 75 |
| . | 76 |
| / | 77 |
| + | 78 |
| - | 79 |
| = | 80 |
| : | 81 |
| ; | 82 |
| [ | 83 |
| ] | 84 |
| \ | 85 |
| { | 86 |
| } | 87 |
| ? | 88 |
| &#124; | 89 |

### Font Generation

The font has been created using FontForge. It uses the PNGs generated from https://bitbucket.org/nickheaphy/f-prismaprepare-datamatrix-stamp-generator/src (they are in the src/DMCodes folder). They have been autotraced using FontForge and no corrections have been made.

This process does generate errors about self intersection when creating the font, however it does not seem to effect the output.

![FontForge](./img/fontforge.png)

### How to Use the Font

Download the font https://bitbucket.org/nickheaphy/f-tecnau-datamatrix-font/raw/HEAD/TecnauDataMatrix_V2.otf and install on your computer.

Within the design application, create a white box and a textbox where you want the barcode to appear. Lookup the program number you require and translate this to a character. Type the character in the TecnauDataMatrix font.

## Note

Use at own risk.

It might be safest to convert font to outlines within the application where you are using it.